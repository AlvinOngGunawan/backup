package com.example.introRedux.services;

import com.example.introRedux.entity.User;
import com.example.introRedux.repositories.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.introRedux.entity.User;
import com.example.introRedux.exceptions.UsernameAlreadyExistException;
import com.example.introRedux.repositories.UserRepository;

import javax.validation.Valid;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KaryawanRepository karyawanRepository;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> getUser() {
        return userRepository.findAll();
    }

    public String deleteEmployee(long id) {
        userRepository.deleteById(id);
        return "Employee removed!!";
    }
    public User saveUser (@Valid User newUser){

        try{
            newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
            //Username has to be unique (exception)
            newUser.setUsername(newUser.getUsername());
            newUser.setGender(newUser.getGender());
            newUser.setAddress(newUser.getAddress());
            // Make sure that password and confirmPassword match
            // We don't persist or show the confirmPassword
            newUser.setConfirmPassword("");
            return userRepository.save(newUser);

        }catch (Exception e){
            throw new UsernameAlreadyExistException("Username '"+newUser.getUsername()+"' already exists");
        }

    }



}
